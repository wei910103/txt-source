這個國家『傑巴古』和『夏斯米盧』一樣，以從迷宮中出產的魔石和魔結晶的中轉地為中心建立的城市。

來訪的只有商人和探索者們。有時也會出現想要討伐竜的「勇者」暴屍荒野。

暗黑迷宮的主人正是暗黑竜『巴魯斯』

先祖蕾特・阿娜亞的同伴，大陸最強的存在。據說其力量超越了神。

───

雖然城市只要支付入市稅就能進入，不過對方還是產生了懷疑。

「不像是商人的樣子⋯探索者嗎？」

產生懷疑也並非無法理解。地獄犬姑且不說，包含著明顯是小孩子的探索者隊伍應該不存在吧。

「嗯，那邊的小姑娘想見見竜。」

卡洛斯聳聳肩說道，守門的人則是一副吃驚的樣子。

「先不說飛竜，竜可不是會經常見到的。就算見到了也會被殺吧。小心點比較好。」

真是很過分的話啊。不過，我也不想在這裡引發騷亂。

───

傑巴古的街道是比夏斯米盧更能體會到力量的街道。

來往的人中有很多都是探索者和有名人，本領也比迷宮都市的人要高。
不知道為什麼，這裡很多人都持有強勁的魔力。看上去像是純粹的戰士的傢伙，恐怕也會使用身體強化系的魔法吧。

向門衛推薦的旅店托管了馬和馬車之後，走向公會。魯道夫預定住在公會的魔獸專用的廄舍中。探索迷宮時也一起帶過去吧。

───

打開公會的門，那裡是異世界。

當然只是比喻罷了。男人和一部分女性怒視著公會的入口，視線冰冷的仿佛會結成堅冰。

第一個進來的是莉雅。
稀世的美少女。自卵中孵化後以來，越發的美麗了。雖然身邊的人並沒有刻意的提起這件事。

可是，對生活在這條街裡的男人來說，那就仿彿是黑暗中閃閃發光的寶物一般。
在前往公會櫃台的途中，一個男人擋住了莉雅的去路。

「小妹妹，來這種地方做什麼呢？」

男人露出一臉下流的表情問著。
莉雅表現出與表面年齡不符的冷靜，只不過，事態向著更加不好的方向發展了。

「和你們沒關係。」

說罷打算從眼前的男人身旁穿過。但是，又有別的男人擋住了莉雅的去路。
此時的莉雅已經忍耐到了極點。
卡洛斯和紀咕慌忙的制止著男人們愚蠢的的行為。在最近的旅途中，他們充分的明白了莉雅的好戰。
只不過，紀咕也是一樣好戰。

「你們這些傢伙，礙到大姐頭了。」

這麼說著的紀咕的巨體釋放著威壓，不過男人們好像什麼都沒有感到的樣子。
感覺麻痺了，還是有著這種程度的實力嗎？這兩種的可能性都很高。

「卡洛斯哥一個人就能簡單的贏了，所以那邊亂來也無所謂喲。」

薩基在鑑定過對方的情報後悠閑地向卡洛斯打著招呼，不過，這個行為更加激起了下三濫們的怒火。

「什麼，小子！」
「你們等級是LV32和LV34吧？卡洛斯哥的等級是LV67哦。」

薩基晃動著手仗，發出聲音。表明使用了鑑定的魔法。

「正好啊，你這傢伙！」
「做給你看，看招！」
「別拔刀啊！！」

從公會的櫃台傳來聲音，不過，看來並不打算制止的樣子。

───

在此之後是相當普通的展開。

卡洛斯和紀咕空手將對方打得破破爛爛的，然後將他們丟到了公會的外面了。

「哎呀，向大小姐學了體術太好了。」
「是啊。」

兩人擊掌慶祝著。
總算是能去進行登錄，不過，又有新的人擋在了前面。

女人。不，即使稱為少女也不奇怪的年齡？
看起來比莉雅要稍微年長一些，不過，一副無所畏懼的表情使人想到了久經戰場的探索者。
一頭仿彿燃燒一般赤紅色的短髮。穿著的鎧甲也與髮色一樣。與漆黑的莉雅形成了強烈的對照。

「你的護衛挺能幹的嗎。」

露出仿彿在對這邊進行評估一樣的視線，不過並沒有讓人感到不快，因為少女就是如此的美麗。
不是護衛，是監督人和弟子，不過，並沒有進行修正的必要。

莉雅觀察著少女的樣子。
少女的腰間吊著一把劍，站立時橫跨的幅度與肩同寬，腰的位置，還有肩線，看來在劍的使用上有著相當的本領，莉雅這麼想著。
如果使用鑑定的話就更加清楚了，不過現在的氣氛不太合適。

（比卡洛斯強嗎？）

「有什麼事嗎？」

雖然對眼前的少女很感興趣，不過先進行登記吧。

「哦，剛才你們打趴下的，姑且是我的部下。」

這樣說著，少女雙眼閃耀著光芒。

「就這樣回去的話面子很掛不住啊。」

真是自說自話啊，對此莉雅立起了兩根手指。

「第一，最初進行找茬的是你們那邊。第二，騎士不砍女人。」

很有道理，但是在這裡並不通用。
誰都沒有阻止，不如說有一群人在起哄。卡洛斯和紀咕相互看了看，誰都不想和少女戰鬥。

「在這裡，最重要的是面子和力量。別在哪裡嘎達嘎達的說個不停，你們裡誰能成為我的對手啊！」

莉雅向身後看去。雙眼噼啦噼啦的放著光。

打心底享受著這個麻煩。
卡洛斯和紀咕對此也只好點頭。

「好喲，我來做你的對手。」

───

這個結果遠在少女的意料之外。

「你嗎？作為小妹妹的對手，這個『真紅的魔劍士』希茲娜的名號有點浪費呢。」
「不知道。」

向對方報上了名號，對方卻只是向這邊靜靜地走來。這過於若無其事的動作，使希茲娜陷入失落之中。
等莉雅回過神的時候，已經是能夠用拳頭對毆的距離了。

莉雅突然揮拳。利用視線死角做出的攻擊被希茲娜格擋開來。
看到攻擊被擋了下來，莉雅順勢回轉身體。

「唔啊！」

瞄準關節進行的攻擊，希茲娜不由得摔倒了。

希茲娜跪在地上仰視著莉雅。看到那充滿得意的臉，希茲娜感到血涌上頭來。

「正好，來真格吧。」

就這樣簡單的摔倒後默默地逃跑的話，自己的面子可掛不住。而且，對方在嘲笑著自己。
這只是希茲娜的誤解。
這是除了露露和卡洛斯以外的同伴們也不知道的，莉雅壊的習慣。

誤認為自己很強卻被打得落花流水的年輕人，還有在卡薩莉雅王國的騎士團和軍隊中，很多人都見過這個表情。
每當這時，莉雅都會露出非常美好笑容。

「變得有趣起來了。」

莉雅一邊開心地笑著，一邊來到公會的外邊。露露抬起頭看向天空，用手捂住了自己的額頭。

───

在行會前的大街，戰鬥開始了。
追來希茲娜將劍拔出。劍身散發著紅色的光芒，是秘銀製的魔劍。

「姐姐，那個劍相當危險。」

更加詳細的情報薩基並沒有說出來。對在與等級１２５級的奧加王的戰鬥中取勝，在那之後更加精進的莉雅來說，在此之上的情報毫無作意義。

而且通過鑑定，那個魔劍對莉雅無效。
圍在大街上的觀眾，比較著雙方的實力。莉雅今天才來到在這個城市，所以評價並不高。

持有稱號的希茲娜，常常因為年紀和外表而被輕視。
由於每次都會發展成互毆或切磋，所以在這個城市名聲逐漸提高了。
但是，至今為止的對手都有著和外表相應的本領。被充其量和自己同歳，身高也和自己所差無幾的少女輕視還是第一次。

「拔劍吧。」

這樣就可以了，希茲娜這樣想著。再怎麼說，對手沒有使用武器的話也太可怜了。
莉雅將刀抽出，刀刃在日光下閃耀著美麗的光芒。
很罕見的曲刀的樣子。不過，看著莉雅的站姿，希茲娜的頭稍微冷靜了下來。

「對了，在這樣的人來人往的地方揮舞武器，不會被巡邏的人被捉住嗎？」

莉雅問著。雖然事到如今有點晚了，不過是很重要事。

「只要不把周圍卷進來的話，彼此之間同意了就算是在戰鬥中死掉也不會有問題的。」

從觀眾中傳來歡呼聲。莉雅一邊笑著一邊點著頭。
那麼就沒問題了。充分地享受吧。

「那，來玩吧。」

希茲娜的臉因憤怒而染紅。

「死吧！」

突然之間全力的突刺過來。
莉雅用虎徹將其化解。
彼此的位置交換了。

「這麼說來，還沒有自我介紹呢。」

莉雅的臉依舊浮現著笑容。但是，並不是在嘲笑對方。

「我的名字是莉雅，稱號是⋯⋯」

只是在享受著戰鬥罷了。

「打倒你之後，慢慢想吧。」