遠征軍開始啟程歸國了。

首先是卡薩莉雅軍，在萊亞斯的率領下，五萬名卡薩莉雅軍開始朝著卡薩莉雅前進。
其中也有少數人打算就此移居奧古斯大公國，而這些請求都被批准了。
而這些人主要都是在這場戰爭中特別活躍的人，因此莉雅賜給了他們以及極少數得她們貴族地位。
其中也有卡洛斯的身影。取了半精靈為妻的他，要在這個不太注重身分的國度生活應該不會太困難。

接著則是奧加軍以及獸人軍。奧加們在享受完戰爭的樂趣後，背著同伴的遺骨一一回鄉了。
相較之下獸人的問題比較麻煩，莉雅依據獸人們的期望賜與了他們相對應得貴族爵位。
不過是在奧古斯領內，跟舊科爾多巴領沒有關係。
面對長年受虐待的獸人們如今竟然晉升為貴族，對此持反對意見的人類很多。

但這個政策是莉雅當初規畫整個大戰略的目標之一。
那就是建立起一個毛茸茸的喵喵王國，還有比這個更重要的事情嗎？沒有。
對於那些反對莉雅政策的勢力，都可以輕易的用軍事力量擊潰。
俗稱打鐵趁熱，因此這些策略必須儘速讓眾人皆知。

而瑪內夏軍其中一半也為了維護瑪內夏國內的安定而歸國。
對莉雅而言比較傷的是魔像兵也全部為了細部調整而歸國了。
雖然是因為維護魔像的設施只存在於瑪內夏國內的關係，但這對戰力而言是非常的傷。
不過留下來的士兵根據其人數以及訓練度來說，其實也還算是充足。
其包括了連戰皆捷的瑪內夏軍以及如今接受莉雅統領的舊科爾多巴兵。

理所當然的產生了叛亂。
但是這些叛亂對莉雅而言完全不算威脅。
莉雅所施行的政策，其中特別是法律部分引起了很大的反彈。

科爾多巴因為長年採取種族歧視政策，而且制訂了許多強烈的法律去規範它。
如今這個政策被取消了。

昨天還遭人蔑視的獸人們如今成為了貴族進入議會中跟眾人平起平坐。
姑且不論有沒有道理，很多人對此無法忍受其實也是人之常情。

但是面對這些持反對意見的貴族們，莉雅用武力強制的排除他們。
這群在莉雅擁有最強大軍事力及權力當下挑起反旗的蠢蛋們，完全稱不上是敵人。

莉雅利用這些叛亂，一口氣將國內的反對勢力一掃而空。
當然也有不少反對派沒有這麼魯莽的採取行動，但是這些會考量利害得失而行動的人們，相對來說要對付他們也比較簡單。
就這樣莉雅廢除種族歧視的政策，在獸人間特別受到歡迎，很多聚落甚至雕刻了莉雅的石像置放於聚落中心，而這些石像持續屹立著直到他們因長年風雨侵蝕倒塌為止。

而莉雅自己離開柯爾納達的日子也到了。
因為長期在外的關係，瑪內夏所累積的政務已經多到不處理不行了。
同時她也送別了長期待在身邊的夥伴們。

首先是瑪露返回了故鄉，而伊琳娜也跟著她一起去了。
因為沒有馬的關係，所以將魯道夫借給她們。

紀咕也一樣返回了奧加村落。
如今的他大幅成長了，已經成長到可以擔任奧加王的對手的程度了。
另外因為要為戰死的奧加們舉辦葬禮，所以他有必要回去一趟。

希茲娜則跟著前往傑巴古的商隊回去一趟。
畢竟她成了女王的妻子，這件事必須跟家族報告。
因此她的臉上充滿的憂鬱的表情。

卡拉跟薩基則是留在柯爾納達。
這是為了如果發生了什麼緊急事情時，可以快速的展開應對。
因為薩基有時空魔法，所以可以即時聯絡莉雅。
另外考量到卡拉的戰鬥力，如果有叛亂產生時應該也能輕鬆對付。
當然，他們其實並不預期會發生這樣的事情。

至於剩下的人則跟莉雅一起回到了瑪內夏。
包括了正式晉升為奧古斯騎士，同時受封為準男爵的卡洛斯以及打算跟他結婚的露露。
現在想想的話，露露是自己來到這個世界後，持續一起活動到自己脫離童真為止的夥伴，是個猶如姊妹般的存在，莉雅一想到她要出嫁，心中就百感交集。
但看著那兩人既害羞又親密的相處談著『我想生個女兒！』之類的事情，讓莉雅決定還是別去破壊氣氛比較好。

菲歐仍然擔任著莉雅的秘書官。
因為已經約定好了，所以必須好好完成跟她結婚這件事。不過『請把女兒嫁給我！』這類的儀式還是得做吧。
但因為目前沒辦法回卡薩莉雅一趟，所以這件事大概得推遲了。

一想到結婚就想起來也該幫松風討個老婆了。
雖然也要考慮松風的意願，但他是如此的名馬。而且身為莉雅的愛馬，應該很容易就能找到一堆新娘後補吧。
就算是那些對馬匹不在行的王宮貴族們也都會送上母馬吧？
沒想到後宮這個名詞根本就是為松風而設的阿。

───

看著薩基站在城牆上眺望著一路遠去的莉雅，卡拉出聲對他打招呼

「你沒跟著一起去，這樣好嗎？」
「這個嘛⋯卡拉大人您才是，不一起去好嗎？」
「因為我在這邊還有事情要解決」

卡拉還是絲毫沒變，臉上帶著溫柔的微笑。
但就如她所說的，她身為莉雅的代理人有著許多事務要處理，另外還有病人跟傷患治療的工作，這些都是非卡拉不可的工作。
另外薩基在這也有非他不可的工作要做。如果又有人掀起叛亂的話，物資運輸工作少了薩基就會變得很困難。

「如果回到瑪內夏，就必須幫忙吉妮維亞大人工作，那實在太辛苦了。還不如在這觀摩卡拉大人您治療傷患，我一邊進行魔法學習及研究」
「這倒是無所謂⋯」

卡拉的手托著下巴沉思著。

「態度太見外了」
「欸？」
「你明明都稱呼莉雅為歐捏醬了，為什麼對我卻是用『卡拉大人』來稱呼呢？」

其實薩基稱呼莉雅為歐捏醬，這是他在晉升為準男爵時，特例賜給他的權利。
也因此沒人可以阻止薩基用歐捏醬稱呼莉雅。但他對卡拉則用不同的方式進行稱呼。

「你也稱呼我為姊姊可以嗎？」

對於這突如其來的攻擊，讓薩基當場呆住了。

莉雅的話，果然還是稱呼為歐捏醬比較好。這是跟稱呼她為大哥相比之下，稱呼為歐捏醬比較好。
卡拉大人就是卡拉大人阿，能直接稱呼她名字的只有莉雅跟吉妮維亞兩人。
至於某些不用卡拉大人稱呼她的閣員們，他們用的是卡拉殿下。
這已經是她身為人的品格高於常人的關係。

「那個，因為我要請您教我魔法，所以師傅或是老師⋯」

這一瞬間卡拉臉上充滿著悲傷的表情。

「那、那就叫卡拉小姐吧。拜託您饒了我吧」
「⋯我知道了，那就這樣吧」

薩基因此鬆了一口氣，像這樣的卡拉還是他第一次看見。
是因為戰爭結束了所以失去了緊張感了嗎？
還是因為跟莉雅分離讓她變得不安嗎？

總之沒有莉雅的生活，讓卡拉產生了不少變化。

───

卡拉在柯爾納達的生活過的很規律。

早上在太陽升起前，進行魔法跟劍術訓練。
早餐意外的吃得很多，接著接受市民陳情，然後面對無盡的公文持續不斷的蓋章。
午餐吃的不多，似乎是擔心吃太多會想睡覺吧。
下午則是巡查市區，訪問孤兒院以及醫院。
晚上則是陪薩基進行魔法研究工作，不過偶爾也會遇到緊急事件而外出。

眾人對卡拉的評價，毫無疑問的非常好。
不管怎麼說，她的美貌、性格、以及治癒魔法都震撼人心。
即使是當初關係險惡的神殿，也因為卡拉將治療場所移到神殿內的關係，而有了大大的改善。
她甚至被神殿稱為『銀之聖女』或是『降臨於此地的最後天使』。不過她本人斷然拒絶了這些稱呼。

───

「就是因為神挑起的愚蠢戰爭，導致了其他大陸慘遭毀滅」

她口中說著的是薩基沒聽過的重要歷史。

「欸？其他大陸？」
「在神彼此間的戰爭下，包括人類在內所有的生物幾乎滅絶了。而殘存的人們，主要以卡拉斯里王國及七都市聯合的人民為主，他們流亡到魔族領地。」

這已經是三千年前的事了，詳細情況已經不清楚了
當時的神之戰也毀滅了這個大陸，後來是傳說中的英雄，聖帝琉庫西法卡跟神竜以及一小部分的神合作，滅掉了其他神才終止了這場戰爭。

「欸？我從來都沒聽過這些事情耶」
「確實呢，因為這些知識都沉眠於魔法學園禁書書庫裡」

在帝都毀滅的現在，這樣的歷史只剩下那裡還保存著吧，或者也可能存留在某些精靈的記憶裡。

薩基覺得，應該要好好的再向暗黑竜巴魯斯討教一番才行。

「但是這些事情讓我知道沒問題嗎？」
「薩基，你總有一天也會去魔法學園的。在那個地方學習，也能讓你腦袋中的前世知識獲得妥善的運用吧」

卡拉這番話，聽起來就如同預言一般。
薩基的確也希望能身處於一個可以跟人互相切磋學習的場所。因為現在他身邊的人都太強了，而且那個強悍是從本質上就不同的東西，所以完全沒辦法作為參考。

「說的也是，等這座城市更加穩定後，就去魔法學園一趟吧」

是的，此時的薩基確實是這麼想的。
但是之後的他，卻是因為完全不同的理由而前往了魔法學園。

佔領政策很順利的進行著。
某些地方雖然仍掀起小型叛亂，但只需要派遣軍隊過去，就能很輕易的鎮壓住。

柯爾納達顯的一片和平。
直到，連續殺人事件發生為止。