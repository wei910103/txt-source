# novel

- title: おっさん冒険者ケインの善行
- title_zh: 大叔冒險者凱恩的善行
- author: 風来山
- illust:
- source: http://ncode.syosetu.com/n9551ee/
- cover: https://images-na.ssl-images-amazon.com/images/I/71gCbyCmraL.jpg
- publisher: syosetu
- date: 2019-12-31T19:55:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- すーぱーぞんび
- 沖野真歩
- 

## publishers

- syosetu

## series

- name: おっさん冒険者ケインの善行

## preface


```
大叔冒險者凱恩，在平時採集藥草的途中，幸運地得到了超稀有物品『蘇生的果實』。
曾一度想要變賣的凱恩，卻遇到一個尋求藥物來拯救同伴生命的少女。
那個身姿，與死去的青梅竹馬少女阿爾蒂娜的面容重疊，凱恩以少許零錢為交換給了『蘇生的果實』。
其實那個少女是被稱作劍姬的最強冒險者安娜斯特蕾亞。
第二天，凱恩突然從S等級的隊伍收到了加入委託，奇跡般的打倒瀕臨死亡最兇惡的魔獸，得到一大筆錢，被未滿20歲的年輕聖女突然告白了！
善良的凱恩原本樸素的生活，從與安娜斯特蕾亞的相遇開始完全改變。

　おっさん冒険者ケインは、いつもの薬草採取の途中で幸運にも、超レアアイテム『蘇生の実』を手に入れる。
　一度は売って金に変えようと思ったケインだったが、仲間の命を救う薬を求める少女と出会う。
　その姿に、死に別れた幼馴染の少女アルテナの面影を重ねたケインは、わずかな小銭と引き換えに『蘇生の実』をあげてしまう。
　実はその少女は、剣姫と呼ばれる最強冒険者アナストレアであった。
　翌日から、ケインのもとに突然Ｓランクのパーティーから加入依頼がきたり、死にかけの最凶最悪の魔獣を奇跡的に倒して大金が手に入ったり、二十才以上も年下の聖女にいきなり告白されたり！
　善良なだけが取り柄のケインの地味な生活は、アナストレアとの出会いから一変することとなる。
```

## tags

- node-novel
- esjzone
- R15
- syosetu
- おっさん（アラフォー
- ささやかな幸せ
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 主人公は人に好かれる
- 優しい世界
- 冒険
- 善行
- 残酷な描写あり
- 男主人公
- 異世界

# contribute

- 岩里政男

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 0

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n9551ee

## esjzone

- novel_id: 1553499716

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n9551ee&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n9551ee/)
- [大叔冒险者凯恩的善行吧](https://tieba.baidu.com/f?kw=%E5%A4%A7%E5%8F%94%E5%86%92%E9%99%A9%E8%80%85%E5%87%AF%E6%81%A9%E7%9A%84%E5%96%84%E8%A1%8C&ie=utf-8 "大叔冒险者凯恩的善行")
- dm5 http://www.dm5.com/manhua-dashumaoxianzhekaiendeshanxing/
- 
