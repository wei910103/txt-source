艾達以有力的目光，環顧四周。

「有聽說過護衛只有二十人嗎？」

我也聽說是三十人，我也是，人家也是，發出了如此聲音。
在遠處的冒険者們也聚集了過來。

「那，有人看到會在之後追上來的十人嗎？」

沒有，才沒有之後過來的人，冒険者們回答了。

「為什麼這裡只有二十人！有人知道原因嗎」

經過了一陣子的沉默後，一位年老冒険者開了口。

「我有問尼齊松。為什麼要減到二十人。尼齊松這麼說了。〈千擊之艾達〉要加進護衛裡。艾達是連〈淨化〉都能辦到的〈回復〉持有者。因為艾達答應會為傷患施展〈回復〉，所以二十人就夠了。尼齊松是這麼說的」
「尼齊松說，我約定會為傷患〈回復〉嗎？」

年老冒険者瞄了尼齊松後說道。

「啊啊，說了」
「我沒有做那種約定！因為做不到！」

艾達以格外大的音量，集中一行人的注目。

「要說為什麼的話，是因為有跟沃卡的領主大人協議過，為了不妨礙神殿，為了不妨礙施療師們，為了守住與斯卡拉貝爾導師的約定，我被人拜託〈淨化〉時一定要收大金幣一枚，而〈回復〉則是金幣一枚！一起護衛的同伴，是我為了讓自己活下去，才施展〈回復〉的！但是尼齊松的傭人並非如此！」

等冒険者們將話語的意義銘刻於心後，艾達問了尼齊松。

「尼齊松。你，真的說了，我約定會施展〈回復〉嗎？」

尼齊松不愧是不好應付的商人。大模大樣地如此說道。

「不不，怎麼可能。沒那種事喔。但是，因為艾達小姐被評價很溫柔，所以我相信，應該是不會對傷患見死不救的」
「我做了約定這件事是謊言。你說了謊」
「沒有說謊喔。只是太相信妳的溫柔罷了」
「那麼，之後過來的十人，什麼時候要會合？」

尼齊松對此辭窮了。然後如此回答。

「不會來」

冒険者們馬上喊起激烈的叫罵聲。

「為什麼不會來」

艾達的聲響低得可怕。

「既然艾達小姐參加了，便覺得二十人就足夠了」
「你是不是忘了？護衛三十人和馬車十五台，是你委託沃卡的冒険者協會的條件。單方面變更，是契約違反」
「現在不就安然無恙地來到這裡了嗎。這代表身為商人的我的判斷是正確的」
「因此讓同伴受了不需要受的傷。運氣不好的話，說不定就死了。你對護衛的冒険者死了幾人都不在意嗎」
「沒，沒這麼說過！」
「大家，聽好了！」

艾達已經放棄跟尼齊松對話，向冒険者們表態。

「記好尼齊松剛剛的來往！然後在回到沃卡後到冒険者協會提供證言！我會安排好，一定會讓他支付這次的報酬！在此之上，我會告發尼齊松！」
「等，等等，艾達小姐。在說什」
「但是！一定會把這商隊平安地送到邦塔羅伊去！這是作為冒険者的我們的自尊心。別在護衛工作上偷工減料阿！知道了嗎！」
「喔！」
「當然了！」
「包在我身上！」
「那麼，大家，回去做野營的準備！各組的組長過來集合！要決定看守的順序！」

尼齊松一臉憤然地移動到馬車去。在喝水壺的水時，白髮的老手冒険者接近了。

「尼齊松。心情很差呢」
「搞什麼啊，那個叫艾達的人。什麼〈北方的聖女〉阿。不就是個暴徒嗎」
「在你眼裡是那樣嗎」
「原本打算到那邊後介紹給有力人士的，這實在是。不會在用那種野蠻的女人了」
「你接下來是打算去哪」
「事到如今說什麼阿。這是去邦塔羅伊的商隊喔」
「你知道藥聖訪問團在回到王都的途中，有經過邦塔羅伊吧」
「那不是當然的嗎。不通過邦塔羅伊要怎麼回王都阿」
「臥病不起的邦塔羅伊領主的母親，被藥聖大人施展的〈淨化〉給治好了，你不知道嗎」
「是有聽過，這又怎麼了」
「在那之後，藥聖大人握著邦塔羅伊領主的手拜託說，請多多關照沃卡的艾達殿，這你知道嗎」
「誒？」
「據老夫所知，邦塔羅伊領主至今給艾達送了使者三次。為了詢問有沒有不自由或不方便」
「那是真的嗎」
「你明明是商人，卻沒掌握情報呢」
「畢竟是跟我的生意無關的事」
「這商隊抵達邦塔羅伊後，艾達一定會被領主招待歡迎」
「誒？」
「你覺得艾達會在那裡跟領主說什麼？」
「那，那個」
「這次的護衛契約事單程。但是，如果領主對邦塔羅伊的冒険者協會下指示，你大概會僱用不了回程的護衛」
「這，這種事」
「就算沒下這種指示，至少，現在在護衛的人是不行的。有看到艾達剛剛的魔弓的射速吧？那救了好幾人。現在在護衛的人都認同艾達的實力。是不會護衛被這艾達告發的對象的喔」
「哼。這種人的話」
「你該不會在想，感覺能省下去程的護衛費吧」
「怎，怎麼可能」
「先別說這個了，這行李能不能在邦塔羅伊賣掉都很難講喔」
「當然賣得了吧」
「要是邦塔羅伊領主禁止了和你交易也行嗎」

尼齊松臉色發青。

「我是為你好。去跟艾達道歉吧。然後要約定好會交出委託達成的硬幣。說到底，跟艾達為敵的話，說不定會沒辦法跟傑尼商店交易喔」
「你，你說什麼」
「你在沃卡開店的日子說不定還很短，但至少要知道傑尼商店跟艾達的關係才行。你去傑尼商店的時候，有被帶領到特別的接待室嗎？」
「特別接待室？庭院深處的漂亮建築嗎？不不，怎麼可能。那是貴族大人用的接待設備吧？」
「艾達去傑尼商店時，似乎會被帶到那裏喔」
「誒」
「說到底，讓傑尼商店飛躍進展的契機之一，就是〈虹石〉把在尼納耶迷宮的最下層得到的物品全部委託給傑尼商店」
「〈虹石〉是什麼？」
「真是的，要從這點開始嗎。會跟你說明的，聽好了」