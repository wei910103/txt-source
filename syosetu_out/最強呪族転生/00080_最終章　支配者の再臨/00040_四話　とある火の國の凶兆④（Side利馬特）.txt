──迪恩那多王国西方的遠處，瑪哈拉努王国。

在神話時代，這個国家由四大創造神一柱的火之神瑪哈爾卜所支配。瑪哈爾卜亡故之際，指定侍奉它的『五大老』五位魔術師之中的都古拉為初代国王，其子孫為繼代国王，而其他四人及其子孫則為王的輔佐，一齊任職在瑪哈拉・拉厄爾宮殿中。如今，瑪哈拉努王国的王與輔佐王的四人共同決定国家的方向，他們被尊稱為『五大老』

瑪哈拉・拉厄爾宮殿的通道裡，一個肩背高大的男人的身影正背靠著牆站著。他身穿裝飾著黃金的華麗外裝，紅色的頭巾下是卷曲的頭髮，銳利的三白眼看著前方。手裡握著象徵著火之神瑪哈拉努的、火鳥姿態的黃金手杖。

他就是擔任五大老之一，被稱為瑪哈拉努王国最強魔術師的利馬特。他在火魔術上的手段，涵蓋到全世界範圍可以說是前無古人後無來者。

通道裡傳來不規則的腳步聲。利馬特聽到後，後背離開了牆壁。

「噢噢，哎喲哎喲，還以為是誰，原來是利馬特閣下麼。呵呵⋯⋯剛才在會議裡真是不好意思，国王瑪古拉也開始警戒迪恩那多王国了噢」

現身的是一位非常矮小的老人。平時他的手腳都藏在衣服裡，如果不露出臉的話，很容易把他錯看成小孩。
他是和利馬特同為五大老之一的吉姆。不過，相比於為了回避戰爭甚至去接觸佩特羅的利馬特，吉姆是個十分極端的過激派。尤其是他經常不與其他人商量而擅自驅使部下行動，將事態復雜化，因此除了利馬特，其他五大老也將他視作問題人物。

「慎重固然很合理、恰當。然而，到了現在這個地步，狀況是不會輕易改變的。加爾沙德王国的上層也開始為應對庫多爾神的復活而行動起來。利馬特閣下也差不多該做好覺悟了。再這樣無理地堅持自己的主張的話，旁人或許會開始猜測，利馬特閣下是否與迪恩那多王国有私通⋯⋯」

利馬特盯著吉姆，沒有回答。吉姆裝模作樣地聳了聳肩。

「噢噢，好可怕、好可怕。請別用這樣的眼神盯著我呢。利馬特閣下的臉真的很嚇人啊，呵呵。這麼討厭我，我和閣下不是關係很好的同事麼。說起來⋯⋯是不是，宮殿內的樣子是不是有點奇怪呢？利馬特閣下有聽說過什麼嗎？」

平時宮殿內會稍微更喧鬧一點。五大老會議的地方、宮殿最上層的『瑪哈爾卜之間』嚴禁五大老以外的人進入，但在其他階層會有宮僕和瑪哈爾卜教的高僧等不少人出出入入。

利馬特在事前與吉姆以外的五大老商議，準備吉姆的暗殺計劃，緊密地調整宮殿內的人員，在當天特定時間段內清空與暗殺計劃無關的人員。

對瑪哈拉努王国來說，吉姆的思想和行為非常有害。得出這個結論的利馬特，之前多次與其他五大老商量對策，打算削弱吉姆的影響力。

但是，要貶低由血統確立的五大老的地位十分困難。馬虎地冷落的話，會引起關係国家權威的問題。而且，貶低五大老的地位是對瑪哈爾卜意志的明確的背叛。因此，要除去地位固守的吉姆，除了暗殺別無他法。

「呵呵⋯⋯無視我麼。會議之外連看都不想看到我麼。利馬特閣下美中不足的地方就是太好懂了，自己的真實想法要好好隱藏噢。閣下實在是太過直率了。作為英雄很相稱，但作為政者也許有點不相稱呢」

吉姆嘲弄著利馬特似的說道。

利馬特為了不讓吉姆察覺，集中注意力地觀察他的樣子。

隨時都可以處理掉他，問題是時機的把握。僅僅是五大老之一的吉姆，他的實力與被稱為人類最強的利馬特有著巨大的差距。但是萬一被他逃脫的話，時間一長這次的暗殺計劃就會暴露，民眾對五大老的信任會被顛覆。利馬特不希望陷入這個最糟糕的情況，所以，他有必要讓吉姆稍微放鬆警惕，然後暴起突襲，一瞬間終結之。

「⋯⋯吉姆閣下，你的想法是否走入了彎路？大邪神庫多爾儘管是屠戮神的惡魔，但並沒有攻擊失去神的国家。這在我等的聖書裡非常清楚地記述著。而且，據說瑪哈爾卜大人也是為了不讓大邪神庫多爾將矛頭指向這個国家，才離開這裡的。這並非需要勞煩瑪哈拉努王国的民眾的問題」
「說過多少次了，還是這個問題：事到如今，你說這些有什麼意義？」

吉姆愚弄似的笑著，從利馬特旁邊走過，背對著他。利馬特瞪著對方的後背。

吉姆的視線離開了。在這個距離快速施展魔術的話，應該可以無抵抗地殺死吉姆。利馬特的魔術具有這種程度的威力、速度、以及無法躲避的規模。

利馬特用力握住金杖。這個時候，意想之外的事情發生了。吉姆猛然轉身，搶先撲向利馬特。他的一邊臉上露出狂笑。

這種事不可能發生。利馬特還沒有做出攻擊的姿態。他不認為剛才的對話引起了吉姆的察覺。

即使吉姆對宮內的樣子有所懷疑、警惕，但他的動作實在太快了。這是因為吉姆本身也企圖以同樣的手段暗殺利馬特，所以才覺察到，這樣想的話一切都能說通。不過，可能性最高的是，五大老之中有背叛者存在。但利馬特現在沒有餘裕思考這些事情。

儘管是發生了意外，但利馬特也歷經過修羅場。他快速揮舞金杖，展開魔法陣。

「নুড়ি 石喲 চূর্ণ 粉碎！」

利馬特與吉姆之間的地面開裂，無數的尖銳碎片向吉姆飛去。吉姆一踢地面高高地跳躍起來，兩腳彎曲地貼到通道的牆壁上。他的動作簡直像是無視了重力。

躲開石之風暴的吉姆一踢牆壁，再次撲向利馬特。地板的裂縫規模猛烈增大，再一次阻斷了吉姆的路線。

「切！」

吉姆躍向後方。但是，地面的裂縫向吉姆延伸，超越了他在他背後擴散開，破壊了周圍的地面。

「嗚⋯⋯！居然做到這個程度⋯⋯！」

吉姆的身影被尖銳的石之彈丸覆蓋。崩壊了地面的裂縫向兩側延伸，進而破壊了通道的牆壁，進一步覆蓋住吉姆。崩塌的牆壁堵住了通道。

「結束了麼⋯⋯但是，想不到五大老之中居然有背叛者⋯⋯⋯看來事情不會輕易完結呢」

利馬特一邊凝視著瓦礫之山，一邊嘆息著。吉姆的動作很奇怪，仿彿知道有暗殺一樣。他儘管是個自負的男人，但竟然會像這樣挑起無謀的戰鬥，真是難以想像。

「難道他連自己的死都計算在內？難道是為了讓我失去地位，而給我附上殺害同胞的污名⋯⋯」
「真是的⋯⋯居然挑在這種狹窄的地方戰鬥，也是挺能幹呢。」

吉姆自然地站在瓦礫之山上，仿彿一直都在那裡一樣。他出來的瞬間連利馬特也沒有看到。

「什⋯⋯到、到底是怎麼回事！我應該是確實地擊潰了你才對！」
「我不是說了嗎？要好好隱藏自己的真實想法啊。利馬特閣下，我什麼時候有把自己的底牌暴露給您呢？放心吧，我不打算逃跑。直到任意一方死掉為止，讓我們盡情戰鬥吧」

吉姆肩背抖動地大笑著。利馬特向上舉起金杖。

「নুড়ি 石喲হাত 具象為手！」

散落在四周的石頭瓦礫聚集起來，形成了兩只巨大的手臂，飄浮在利馬特的前面。

「我實在是不想做出破壊瑪哈拉・拉厄爾宮殿的事情，但也沒辦法了」