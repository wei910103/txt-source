在冒險者公會，我被告知教授魔法的教會，在城鎮的邊緣。用ＭＡＰ檢索，馬上就找到了。

怎麼說呢，與其說是有韻味，不如說是讓人感受到歷史的氣氛。嗯，說白了就是破爛。牆壁瓷磚的剝落感覺不錯。屋頂不是傾斜的嗎？

窺視裡面，孩子滿滿的！在外面玩耍的小孩子們，總覺得被親近地纏住了。

手牽著孩子們向裡面走去。側目凝視著手插入口袋，靠在牆壁上無感情地旁觀的孩子們。

「你好。神父大人在嗎？」
我試著小聲的問了。

「有什麼事嗎？」
大概３０歲後半吧。穿著像教會的衣服，溫柔的男性出現了。

「其實我想學回復魔法。冒險者公會介紹我來的。」
「這樣啊。那麼，學費需要２０枚金幣。」

神父說。也許是心理作用，看上去很累。不是心理作用吧。幾乎看不到像修女一樣的人。有的只是孩子。

「現在手頭的現金很少，我去籌措一下。」
說完就離開了教會。好貴啊～。但是有那麼多孩子啊。只要這點學費就夠了嗎？

在兌換所轉了幾圈，用各２０張金板，在５處分別兌換了４０枚金幣。總共得到了２００枚金幣。

在街上，到處是各式各樣的食物。一邊難得地眺望著第一次看到的中世紀風格的街道，一邊努力地取得這邊的物品。在地球上時，也沒有去過歐洲。

商店說起來有日本昭和的味道，就是這樣的感覺。蔬菜屋啦，三河屋啦，形態什麼的。

像油炸麵包、水果、串燒、餅乾一樣的點心。袋裡有各種布料和線和針。食用油，蔬菜，羊皮紙，筆墨，之後發現了糖果。

各種乾肉加湯料。衣服和內衣還有涼鞋等等。各種各樣的戰利品，道具箱變得豐富起來。

從下午開始詢問，立刻請求了魔法的教授。我捐了很多東西。孩子們發出了歡呼聲。

飯還沒好嗎？不，好像沒有午飯。我好像也明白了這件事。

捐贈的食物轉眼間不見了。神父的苦笑給我留下了深刻的印象。公會的人知道這個現狀而介紹給我。

對這個世界似乎也有的親切稍微感到溫暖。這段時間太殺氣騰騰了。我的心好像病了。

從白天開始就讓肚子吃飽的小孩子睡覺，終於開始了魔法的講習。剩下金幣１９２枚，銀幣８枚，大銅幣３０枚，銅幣４５０枚嗎？

講習也不能免費進行。孤兒院擁擠不堪。好好地作為助手接待了客人。來了一個骨折的人。神父發動了魔法。

目不轉睛地看。一邊解析。魔力聚集到眼睛的感覺。從神父的身體深處發出魔力的氣息。記住了！

而且，隨著咒文詠唱，從手臂上發出光芒，看起來就像光離開了一樣。我想只是看起來那樣的東西。感覺是用魔力看到的，所以才會這樣吧。

魔法PC將那個過程整合，自動製作了技能的包裝。這多虧了【那傢伙】啊。

看狀態的話好像下面。

全屬性魔法
回復魔法【治療Ｌｖ１】

然後，在技能欄裡【見解Ｌｖ１】

太好了，記住了。總覺得有個奇怪的技能。似乎是把學習魔法的步驟，總結成一個技能出來。這無所謂。不過，有的話一定會更輕鬆。

患者的骨頭好像連在一起了，但是好像沒怎麼收治療費的樣子。

下一個病患來了。
【治癒Ｌｖ１】、【清血Ｌｖ１】

接著中毒的人來了。
【解毒Ｌｖ１】

其次是輕症。不過，大量的傷患一個接一個地進來。
【區域治療Ｌｖ１】

接著又來了中毒的重傷者。
【中級治療Ｌｖ１】、【中級解毒Ｌｖ１】

其次是重病人。
【中級治癒Ｌｖ１】、【中級清血Ｌｖ１】

因為受傷，症狀不佳，有人不得不繼續康復。這個好像是使用時要注意的魔法。
【漸癒Ｌｖ１】

有因為食蟲毒的症狀而被集中運進來的人。
【區域漸癒Ｌｖ１】

「好厲害啊。只給您看了一遍。這種事很少發生。哦，你全部都記住了嗎？恭喜你。第一次有學得這麼快的人。」

「謝謝你。這樣就可以安心地旅行了。」
恭恭敬敬地道謝後回去。太好了，終於實現得到魔法的心願。感謝回復魔法。

哈。村子裡有很多受傷的人吧！？明天帶很多東西去看看吧。

我又去買東西了。食品、服裝、其他各種物品。在家裡稍微地複習魔法。