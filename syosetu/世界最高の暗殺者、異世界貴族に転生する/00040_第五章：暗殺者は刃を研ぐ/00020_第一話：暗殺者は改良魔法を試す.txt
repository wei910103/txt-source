回家後，馬上出去狩獵。
按照慣例，為了不破壞領民的狩獵場，在深山裡進行狩獵。

「不管怎麼說，我的妹妹嗎？」

一般來說，有魔力的女性很難生孩子。
第一胎雖然沒有辛苦地出生，但第二胎以後卻突然變得困難。醫學上原因不明，被認為是神的意志。

如果不是那樣的話，現在會有更多的魔力持有者吧。
畢竟，魔力持有者的人數直接與國力相連，國家和貴族們都有意識的想留下很多子孫。

無論怎麼努力，最多也只有兩三個孩子。這樣的話，如果減去病死、事故死、戰死等部分的話，擁有魔力的人口就會減少，但很少非魔力的人也會產生擁有魔力的人，所以這幾十年來，魔力持有者的人數量沒有太大的變化。

生孩子這麼困難卻有了第三個孩子，年輕的母親姑且不論，我覺得父親太努力了。
他們倆現在還很恩愛。

「⋯⋯家人增加也不壞啊」

最初很驚訝，現在變得很期待。
同時我變得不能死了。

只要我是托瓦哈迪，妹妹就能夠作為普通的貴族生活下去吧。
但是，如果我死了，就會變成下一個托瓦哈迪。
我想避開那個。

出於父親的性格，作為我的保險，從小就接受暗殺術，但我打算說服並阻止他。
雖然不能對父親說，但如果我死了的話，誰也無法阻止埃波納，世界毀滅了。我不需要我死後的保險。

「好了，找到了。阿爾凡兔，今年森林很豐富，長得很肥呢⋯⋯迪雅會很開心的」

一邊進行狩獵，一邊嘗試改良版的風屬性探索魔法。
只要在風中融入意識，擴大知覺範圍，是我最喜歡的魔法，只要在有風的空間，誰都無法從我身邊逃脫。
我把那個版本升級了。

一直以來，都是以自身為中心擴大圓圈的印象。
因此，隨著效應範圍的擴大，負擔呈指數函數增大。
想像一下圓就知道了。假如半徑為一米的圓，面積大概是三平方米，如果是二米的話就是十二平方米。只增加了九平方米。
可是，如果把半徑增加的話，探索麵積也會進一步增加。

總之，只要採用擴大圓的方式，擴大搜索範圍就非常困難。
因此，在改良版中改變了方式。
不是圓形，而是直線延伸。把那條線以自己為中心旋轉。這樣的話，雖然持續探索的範圍有限，但魔力負擔只需要以往幾十分之一，而且可以擴大探索範圍。

「雖然有弱點」

在使線旋轉的性質上，與以往不同，並不總是看著搜索範圍。
將線旋轉３６０°所需的時間大約為０.１秒。雖然還不到０.１秒，但還是有遺漏。
話雖如此，一般情況下這種超短時間的遺漏成為問題的情況很少。
然後，到了緊要關頭分開使用就行了。
如果要作為接近戰鬥的補助，０.１秒是致命的，不過，那個情況以以前的圓形展開就行了。如果是接近戰鬥的話，展開範圍就不會太大了。

「那麼，狩獵嗎？」

從【鶴皮袋】中取出十字弓。
槍的射程和威力都很強。但是，槍威力過高，會導致肉嚴重破損。如果想得到稍微好一點的肉，最好是這個。
解開裝填在十字弓上的箭，裝填新的箭。
因為十字弓是以用於暗殺為前提考慮的，裝填了塗有毒藥的箭的狀態下裝入收納袋。
用這種東西殺掉的話，在訓練中獲得毒性的我和父親雖然無所謂，但其他的人會吃到吧。

因為覺得十字弓很方便，所以這樣準備著。沒有聲音。我認為比起槍更適合暗殺。
擺好架勢，放箭。
在樹木縫隙中飛翔的箭，正如其意，刺中了阿爾凡兔的脖子。

「首先是一隻兔子」

阿爾凡兔是大型的兔子，有中型犬的大小，可以吃的很過癮。
但是大家都很能吃，還想再來一隻。


◇

狩獵結束後下山。

今天的成果是兩隻阿爾凡兔和兩隻野豬。而且收集了一筐蘑菇和野菜。要感謝【鶴皮袋】

背著這麼大的行李下山非常麻煩。
解體後，分給領民們吧。
姑且不論阿爾凡兔，吃掉一頭野豬是很辛苦的。
不知不覺到了秋天。
差不多到了必須要注意過冬的時候了。


◇

解體後，把太多的野豬和兔毛皮和肉交給村裡的人。

肥美的野豬肉對民眾來說是美味的佳餚餚，阿爾凡兔的毛皮柔軟且溫暖，冬天臨近的話能高價賣出，所以得到了人們的喜愛。
作為回禮收到新鮮的蔬菜。這個也讓我用在明天的宴席上吧。

然後，現在是廚房。
雖說明天要做飯菜，但也有必須要準備的東西。
一部分調味料等攙和在一起擺一晚比較好。尤其是和野豬肉味道相配的香料。
這樣預先準備，菜就更美味了。

「我回來了，媽媽和塔爾特都很興奮呢。真意外，迪雅也在嗎？」
「沒想到這麼意外。因為我也想學會做飯」

迪雅鼓起臉頰。
原本以為娜芳和迪雅是專門吃的，但是很少見的迪雅也在幫忙。
不過，目前看來很難說已經是戰力了
繼迪雅之後，媽媽和塔爾特也轉向我。

「啊，歡迎回來。不愧是羅格。這麼短的時間，這麼多看起來美味的獵物」
「阿爾凡兔和野豬。看上去都很好吃」
「啊，那個肉是阿爾凡兔啊！？做燉菜吧，還有奶汁烤菜！那個，以前羅格做之後最喜歡吃的東西」
「阿爾凡兔打算做成燉菜和奶汁烤菜。還要敲打野豬」

瞄準了阿爾凡兔，是因為迪雅喜歡吃奶油燉菜和奶汁烤菜，所以本來就打算這樣。
並且，敲打野豬也是兼做新魔法實驗的。

「羅格，在敲什麼呢？不知道的烹調方法」
「這是明天的樂趣。⋯⋯你準備的是盧南鱒魚的麥糠醃菜嗎？」
「沒錯。羅格和基安都喜歡」

兩個人做的是魚料理。
托瓦哈迪有一個大湖，經常吃魚料理，鱒魚的一種，盧南鱒魚等對我來說是家鄉的味道。

托瓦哈迪自古以來就為了不讓大自然的恩惠斷絕而設置了捕魚限制，產卵時期等也禁止捕魚。
因此，為了確保在不能捕魚的時期吃的東西，魚的保存技術很發達。

起初好像只考慮保存的事，不過，到了祖父這一代左右，托瓦哈迪變得富裕起來，開始拘泥於如何讓它變得美味了。
托瓦哈迪製造的盧南鱒魚乾物等都是用合乎情理的手法做的，與那些五顏六色的東西劃清界限。
賣到哪裡也不覺得丟臉的東西，在商業城市穆爾特也是很受歡迎的。

並且，兩人做的應該是盧南鱒魚的麥糠漬。用麥子做的米糠醃魚，是托瓦哈迪獨特的地方菜。
這樣做的話，不僅可以保存，味道也會加深。蒸熟米糠醃過的盧南鱒魚是絕品，民眾習慣在特別的日子吃。
將魚用米糠醃製也許有些奇怪，但在轉生前的世界裡，將肉和魚用米糠醃製並不稀奇。因為原理上和鹽醃製沒什麼兩樣。

「哇，是極其完美的盧南鱒魚。尺寸不錯，油脂也很好」

是特上品。這麼多的盧南鱒魚並不多見。

「漢斯先生給我的祝賀。這麼好的東西蒸一蒸！因為蒸的話醃漬一夜就好了，所以和塔爾特一起做喲。很期待呢」
「啊，一定很好吃。只是⋯⋯對於其他領地的客人評價很差啊。米糠醃製的盧南鱒魚用蒸的」

毫無疑問，與生吃相比，美味成分在增加。味道是好的。
只是，因為發酵食品的宿命而是散發著獨特的氣味，很多人都認為這樣是不行的。或者說，即使是托瓦哈迪的民眾也有不擅長的。
娜芳肯定是第一次，迪雅也沒吃過吧。
我擔心她們會不會產生抗拒反應。

不是蒸，比如大量加入辛香料翻炒的話，糠味也會消失，但如果是這麼漂亮的盧南鱒魚，那樣調味就太可惜了。我想簡單地品味素材的好處。

「呵呵，不行。決定做蒸魚。不明白這種味道就不能成為托瓦哈迪的女人！大體上，羅格和基安最喜歡的食物做成別的料理是很罕見的！」

有一番道理。
但是，我想還是同時放一個緩衝讓他們習慣比較好。
那麼⋯⋯⋯

「媽媽，蒸魚可以交給我嗎？」
「⋯⋯一定有什麼企圖吧」
「沒那回事，在穆爾特學到了非常美味的蒸魚方法，也想讓媽媽嘗嘗。好厲害，把魚的美味完全封閉起來，滋潤一下。我想說至今為止吃的蒸魚是什麼。我想如果用這種盧南鱒魚的麥糠漬來做的話，會變成非常美味的佳餚的」
「嗯，被這麼一說，我開始感興趣了。好極了。但是，我們約好了。一定要做成蒸魚」
「啊，交給我吧。」

我微笑著。
其實是在穆爾特學到的是謊言，其實是前世學到的技術。
據我所知最棒的蒸法。
那樣的話，媽媽也會高興的，迪雅和娜芳也會覺得好吃的吧。

原本是偽裝成廚師身份，為了接近暗殺對象而掌握的廚師技能。
現在這樣，為了讓母親、戀人、朋友高興而行動，真是不可思議啊。

第一次的人生作為道具而活著。
但是，我可以挺起胸膛說人生並沒有白費。

正因為有了第一次的人生，才能掌握各種各樣的技能，才能讓她們露出笑容。