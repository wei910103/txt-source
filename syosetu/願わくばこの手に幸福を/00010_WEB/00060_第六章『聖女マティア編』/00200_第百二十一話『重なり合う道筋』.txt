醒來的時候太陽已掛在半空，已經是中午了啊..
將裝在陶器裡的朗姆酒倒在嘴裡，一口氣吞下。在睡不著的日子裡，這麼做是最好的。對布魯德來說，那幾乎就是每天的必修課。

這麼多年來，甚至沒有一天能安然入睡。所以，我決定酗酒，讓混濁的朗姆酒流入喉嚨，越是劣質的朗姆酒，越讓我頭痛，但這樣就可以更快地入睡了。

你趕緊去死吧，被這樣說過很多次。但也沒什麼大不了的，我從來沒有想過要活很久，也沒想過在床上安詳地死去。
這樣的日子，如果不把一切都拋到腦後，就活不下去了。我突然想到，也許那傢伙也是如此。

看著在便宜的床上熟睡的男人。一點起床的跡象都沒有。嘛，那倒也無所謂，這裡也有相應的準備。

名字是路易斯，隸屬於紋章教的叛徒，傳聞是大罪人，但怎麼也看不出來。莫非是認錯人了？之所以這麼想，是因為他那種魯莽的態度。

貝爾菲因的統治者，守護者，鋼鐵公主薇斯塔莉努。違逆她什麼的，和找死沒有區別。當然，我也沒有資格說他就是了。

再加上昨天晚上他喝醉後說出來的話。
“內容是——貝爾菲因的兩輪，取下其中一環。就這些。”
聽到的瞬間，我動搖了。還記得茶色的頭髮在視野中跳動。是真心的嗎？是醉後發牢騷呢，還是藏在心底的真心話呢？我不知道。也不知該不該問。
但是，有一件事可以確定，只有活膩了的人才會考慮這件事。

這個叫貝爾菲因的城市既複雜又精緻，既簡單又複雜。沒有一介傭兵插足的餘地。
布魯達想，這個傢伙，路易斯也一樣吧。雖然他有些本領，但我不認為他是能輕鬆立足於這座城市的人。其表情中也沒有浮現出強者的自信。

那麼，他一定是在做夢。
布魯達認為如果只是做夢的話，陪他一起也無妨？夢想什麼的只要隨心所欲就好了。總有一天會連沉溺在那個幻想都無法實現，也會在此期間放棄一切，甚至放棄活下去的念頭。

自己，對，自己也一定，是那樣。布魯德一邊用朗姆酒洗臉，一邊清理地板。深深的嘆息，快要從內心深處涌出來了。
自己沒有在這個世界生存下去的氣力。過著毫無意義的日子，毫無意義地吃飯，毫無意義地喝酒睡覺。

只是這樣，這就是人生。人生就是惰性。

過去，父親被稱為好友的男人背叛，失去了生命和尊嚴，同時自己也失去了妹妹和一切。從那天開始，這樣的生活就沒有過任何改變。
我真的很羨慕那些能把內心燃燒殆盡進行復仇的人。自己連那種氣力都沒有，只是行屍走肉般地活著。

布魯達的鼻子微微抖動。和往常不一樣的氣味，從房間裡傳了出來。而後又一次發出嘆息，看了看床，路易斯還閉著眼。
為什麼讓那樣的人留宿了呢？男人自不必說，連女人也沒有住進來過。不，我從來沒想過讓別人留宿。為什麼？

這對於布魯達實在是太不可思議了。正因為是自己，所以更加…

◇◆◇◆

當眼睛慢慢睜開時，布魯德已經不在房間裡了。太陽與其說是從東邊升起，倒不如說差不多該向西傾斜了。啊啊，睡得真熟。

我覺得自己並不是積攢了太多疲勞，只是偶爾會有奇妙的睡意襲來罷了。哈欠沿著喉嚨從嘴唇漏了出來。
昨晚，很難說從布魯德那裡得到了期望的答覆。一邊思考著，一邊嘟噥著“原來如此”。一邊把朗姆酒吞下肚去。

我不知道他在想什麼，他也沒有說出口。而且，我明白傾覆這座城市，並不是那麼輕易就能接受的委託。
胃，緩緩地搖晃著。心中有一種奇怪的不安在跳動著。啊，這樣啊，我很不安啊。

事到如今，我明白了。儘管自己沒有意識到，但果然還是在潛意識裡依賴著布魯達啊。
既是曾經的朋友，又是伙伴，還是引領著我的存在。我果然還是依賴著他。

實在是太沒有出息了。

為了尋找能幫助自己成為英雄的東西，才獨自來到此地。是為了能用自己的腳踩在地上，為了得到什麼才來到貝爾菲因的吧？
可是，我卻不由自主地依賴著他。啊，真是糟糕透了。這和以前的時候一樣，和對處於死亡旋渦中的布魯達見死不救的時候沒有任何改變吧。

真讓人厭惡。果然，人是不會輕易改變的。更何況，在這種時候，拜託布魯德這件事本身，不就證明了我什麼都沒有改變嗎？
大腦隱隱作痛，對自己的厭惡使得心如刀絞，臉上浮現出扭曲的笑容。

不行啊。好像醉得很厲害。今天心情比平時更奇怪。呆在房間裡，應該不會有什麼好事。
正當這時，房門吱吱地響了。看樣子，剛才應該是敲過門了，不過好像沒聽見。
“客人。您的同伴等著呢。快點來吧。”
門對面傳來老婆婆沙啞的聲音，似乎有些郁悶。
同伴麼，可能是布魯達吧，真稀奇。

比起等待別人，他更像是自己一個人埋頭往前走的男人。
但是，既然在等我，就不該讓他白等。

雖然現在還不知道到底該和什麼樣的人見面，外出很危險，但那傢伙肯定還是要去喝酒的。
算了，那樣還能讓自己心情舒暢點。這麼想著，我踏著地板走出了骯髒的房間。

所謂紅燈區，絲毫不在意床之外的地方，所以總覺得地板、門之類的木頭都已經腐朽了。到了夜裡，讓人睡不著的噪音就會襲來。我也明白了會把空房間低價租出去的原因。
一邊披著上衣，一邊輕輕地伸手，眯起了眼。

站在門口的那道身影，進入了瞳孔。除那之外，沒有其他等待著的人了。老婆子偷偷地看向這邊。毫無疑問，那個人就是在等我的人吧？
“還真是老樣子，好身份啊。一個人默不作聲地離開了，然後在這種地方一覺睡到現在，真讓人吃驚。”
長髮綁起來掛在腦後，沒有遮住凜然的眼神，浮現出一絲微笑的她。不會吧，怎麼會有這樣的事…
紋章教的首腦，他們的旗幟，聖女瑪蒂婭的身影，不合時宜地站在那裡。

妓院這個地方和她太不相稱了。但即便如此，她還是沒有失去光輝，周邊的環境反而更突出了她的存在。

原來如此。看到那個身影的瞬間，我領悟了，我好像遇到了最壞的情況。