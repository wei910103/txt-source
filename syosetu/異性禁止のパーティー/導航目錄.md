# CONTENTS

異性禁止のパーティー  
ミルク多めのブラックコーヒー  
組建了異性禁止的隊伍，但我們的成員有些不對勁  
異性禁止のパーティーを作ってみたけど、ウチのメンバーどこかおかしい。 ミルク多めのブラックコーヒー  

作者： 丘野　境界  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E7%95%B0%E6%80%A7%E7%A6%81%E6%AD%A2%E3%81%AE%E3%83%91%E3%83%BC%E3%83%86%E3%82%A3%E3%83%BC.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E7%B5%84%E5%BB%BA%E4%BA%86%E7%95%B0%E6%80%A7%E7%A6%81%E6%AD%A2%E7%9A%84%E9%9A%8A%E4%BC%8D%EF%BC%8C%E4%BD%86%E6%88%91%E5%80%91%E7%9A%84%E6%88%90%E5%93%A1%E6%9C%89%E4%BA%9B%E4%B8%8D%E5%B0%8D%E5%8B%81.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E7%B5%84%E5%BB%BA%E4%BA%86%E7%95%B0%E6%80%A7%E7%A6%81%E6%AD%A2%E7%9A%84%E9%9A%8A%E4%BC%8D%EF%BC%8C%E4%BD%86%E6%88%91%E5%80%91%E7%9A%84%E6%88%90%E5%93%A1%E6%9C%89%E4%BA%9B%E4%B8%8D.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu/異性禁止のパーティー/導航目錄.md "導航目錄")




## [席爾瓦離開隊伍](00000_%E5%B8%AD%E7%88%BE%E7%93%A6%E9%9B%A2%E9%96%8B%E9%9A%8A%E4%BC%8D)

- [不參加這樣的隊伍了！（上）](00000_%E5%B8%AD%E7%88%BE%E7%93%A6%E9%9B%A2%E9%96%8B%E9%9A%8A%E4%BC%8D/00010_%E4%B8%8D%E5%8F%83%E5%8A%A0%E9%80%99%E6%A8%A3%E7%9A%84%E9%9A%8A%E4%BC%8D%E4%BA%86%EF%BC%81%EF%BC%88%E4%B8%8A%EF%BC%89.txt)
- [不參加這樣的隊伍了！（下）](00000_%E5%B8%AD%E7%88%BE%E7%93%A6%E9%9B%A2%E9%96%8B%E9%9A%8A%E4%BC%8D/00020_%E4%B8%8D%E5%8F%83%E5%8A%A0%E9%80%99%E6%A8%A3%E7%9A%84%E9%9A%8A%E4%BC%8D%E4%BA%86%EF%BC%81%EF%BC%88%E4%B8%8B%EF%BC%89.txt)


## [初心者訓練場の戦い](00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AE%E6%88%A6%E3%81%84)

- [初心者訓練場にて](00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AE%E6%88%A6%E3%81%84/00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AB%E3%81%A6.txt)
- [糾纏而來的惡意](00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AE%E6%88%A6%E3%81%84/00020_%E7%B3%BE%E7%BA%8F%E8%80%8C%E4%BE%86%E7%9A%84%E6%83%A1%E6%84%8F.txt)
- [交渉と前準備](00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AE%E6%88%A6%E3%81%84/00030_%E4%BA%A4%E6%B8%89%E3%81%A8%E5%89%8D%E6%BA%96%E5%82%99.txt)
- [收集來自被害者們的情報](00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AE%E6%88%A6%E3%81%84/00040_%E6%94%B6%E9%9B%86%E4%BE%86%E8%87%AA%E8%A2%AB%E5%AE%B3%E8%80%85%E5%80%91%E7%9A%84%E6%83%85%E5%A0%B1.txt)
- [油断大敵](00010_%E5%88%9D%E5%BF%83%E8%80%85%E8%A8%93%E7%B7%B4%E5%A0%B4%E3%81%AE%E6%88%A6%E3%81%84/00050_%E6%B2%B9%E6%96%AD%E5%A4%A7%E6%95%B5.txt)

