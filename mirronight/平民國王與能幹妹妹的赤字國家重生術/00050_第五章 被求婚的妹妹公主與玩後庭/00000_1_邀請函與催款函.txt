「祭主大人答應了。」

和我在溫泉做愛完，過了剛好兩天的時間，夢絲在圓桌會議向所有人報告。

「答應......是指觀光客能夠參拜神殿嗎！？」

「是的，她同意能夠參拜祭壇。但是，不可踏入內殿觀看秘儀。只有即位儀式或王室的例行儀式，才能讓接受招待的客人進入內殿。」

在場文官一片喧嘩。

而我事前就聽過羅絲報告，才能仔細觀賞文官們一個個驚呆的模樣。羅絲見狀也露出愉悅的笑容。

「您成功見到祭主大人了嗎？究竟是如何做到？」

「我們在公眾溫泉見面的。」

「夢絲公主，您親自去公眾溫泉嗎！？」

「是的，我拜託間諜去打探祭主大人的動向，得知祭主大人會在黎明時，去神殿附近的溫泉放鬆身心，只有在那時候我才能和祭主大人說上話。」

「我國的公主，竟然去了庶民的公眾浴池！」

文官們被突如其來的驚人事實弄得暈頭轉向。

「泡澡時無須拘泥禮數，要請求祭主大人，就只能趁這個時候了，祭主大人真的是位非常出色的女士。我邊幫祭主大人洗背邊聊著，就輕易取得她的認可了。不僅如此，她還說若有什麼困擾，隨時能找她商量。

「太厲害了......不愧是蘿絲公主......就連離宮所有權一事，也是多虧她說服了貴族們。」

「呵呵，還有一件事。她向大陸上所有的歐墨尼得斯教會發出信函，說『歡迎前來參拜，想進入神殿參觀也沒問題。』」

文官們一同歡呼鼓掌。

「公主殿下！您實在是太厲害了！」

「這樣終於能招攬觀光客了！」

「況且儀式只要在新神殿再現就好了。」

「關於這件事......」

我站起發言。

「新神殿的工程大致完成了七成，差不多要找些熟練的工匠來完工，只是人事費和材料高漲，錢完全不夠用。我想向過去倒債的外國寄出催款函。」

「屬下擔心這樣沒用，過去已經寄了無數次。」

「那是因為前國王陛下隱瞞病情，使得判斷能力降低導致的結果。雖說當時取得陛下的許可，但真不該借出那筆錢，這都是我等文官們的疏失。」

「將我即位儀式的邀請函和催款函一同寄出。」

「什麼——向那些倒債的國家嗎！？招待他們參加即位儀式！？不可能！這分明是向搶匪討錢啊！？」

賽巴斯汀發出哀號。

「只有在王族的儀式，才能招待客人進入內殿。內殿是離歐墨尼得斯女神最近的地方，還能觀賞真正的巫女舞來延年益壽，任誰都會想要去吧？哪怕是把倒債的款項還清也得去。」

我不懷好意地笑著。

建築工作伴隨著大筆的金錢流動，其中也常碰到賴帳的客人。

碰到那種客人，我就會去包下馬戲團，然後只招待那些爽快付錢的客人觀賞表演，想從付錢不乾脆的客人手中拿到錢，這個方法是最棒的。

「他們有沒有可能只接受招待，最後還是不還錢？」

「不還錢他們哪有臉見女神？」

會議室中一片寂靜。

只聽到在場者發出感嘆的聲音。

「這說不定行得通啊。」

「就試試看吧！」

「不愧是王兄！」

「這都多虧蘿絲說服了祭主大人。」

「在下立刻製作邀請函和催款函並派使節送去。」