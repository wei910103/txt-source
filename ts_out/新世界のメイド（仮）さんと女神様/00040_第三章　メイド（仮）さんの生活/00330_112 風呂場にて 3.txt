米卡埃菈達はそれぞれ、シャツの前を開きかけだったり脫いだズボンを手に持ったままだったりと中途半端な格好で、三人の姿に麻里子は目を瞬かせた。巴爾特と托爾斯頓が男湯あちらにいるのだから、この三人が女湯こちらにいること自體はおかしくないどころか必然と言ってもいいことである。ただ、どうして脫ぎかけの姿のまま動きを止めているのか、麻里子には理由が分からなかった。

「カリーネさん達、何やってるの？」

麻里子と三人が圖らずも無言で見つめ合っていると、麻里子の手を引いていた亞里亞も不思議に思ったらしく、麻里子の疑問を代弁するかのように聲を上げた。その聲にようやく三人は動き出した。

「え、ええとね⋯⋯」
「あー、嚯啦、あれだ。ねえカーさん」

青い髮の珊德拉が口ごもり、赤毛の米卡埃菈が舉動不審氣味に綠の髮のカリーネに話を振った。カリーネは一瞬、え、私？　という顏をした後、ゴホンと一つ咳払いをすると亞里亞に顏を向けた。

「別に何かしてたわけではありませんよ。お風呂に入る準備をしていたら向こうが何か騒がしくなったでしょう？　何事だろうと樣子をうかがっていただけですから」
「それは⋯⋯、ごめんなさい、哈薩爾が騒いじゃって。もう、あの子が素直に言う事聞かないから」
「ああ、それは氣にしなくてもいいわよ。哈薩爾君の氣持ちも何となく分かりますからね。托爾斯頓達と行ったのなら、任せておいても大丈夫よ。それよりその方が⋯⋯麻里子桑？」

カリーネはそう言うと顏を上げて、亞里亞から麻里子に視線を移した。亞里亞もつられて手をつないだままの麻里子を振り返る。

「おねえ醬のこと？　あれ？　さっきまで皆食堂にいたんでしょ。會ってなかったの？」
「ええと⋯⋯⋯直接お話はしていないですね」
「そういえば確かにそうだな。ああ、でもそれは私のせいかも知れぬな」

亞里亞に答える麻里子に、橫から米蘭達が口を挾んだ。米蘭達が阿朵蕾に近づくのを避けたため、麻里子が給仕に出る時は逆に阿朵蕾のテーブル付近ばかりに向かうことになったのだ。結果として、麻里子は直接巴爾特達のテーブルには行かずじまいになっている。

「では、この機會に紹介しておくこととしよう。カリーネ殿、こちらが麻里子殿だ。一昨日、新たにこの宿での住み込みとなられた。私と同じ立場だな。で、麻里子殿、こちらの三人は巴爾特殿の組パーティーに屬する面々だ。順に、カリーネ殿、米卡埃菈殿、珊德拉殿だ」
「ち、ちょっと待って、米蘭達！」

米蘭達が手早くそれぞれを紹介したところで、カリーネからストップが掛かった。三人とも、あわてて服を著なおそうとしている。元々、服を脫ぎかけた狀態のままで話をしていたのである。ちょっとした話だけならともかく、初對面の挨拶を交わすのにふさわしい姿とは言えないだろう。麻里子はそっと目を逸らして見ていない風を裝った。

「どうせこの後、皆脫いで風呂であろう。氣にされずともよいではないか」
「そういう問題じゃない！」
「米蘭達は大雜把すぎだよ」
「米蘭達桑⋯⋯」

麻里子の氣遣いをぶち壊す米蘭達の言は皆には受け入れてもらえなかった。


◇

「では皆桑は、もう四年も探險者探險者をなさってるんですね」

麻里子の聲に三人が頷く。

三人が衣服を整えたところで改めて麻里子と挨拶を交わし、今は簡單に互いの身の上話を披露し合っているところである。麻里子の記憶云々の話が米蘭達の口から傳えられた時には、三人から同情と若干の興味のこもった眼差しを向けられた。

巴爾特達五人は同郷で、他の三人より一つ下の米卡埃菈と珊德拉が成人するのを待って探險者探險者になったと言う。カリーネと男二人は今二十歳だということだった。

「さあ、立ち話はこれくらいにして、そろそろお風呂に入りましょう。このままだと男性陣を長いこと待たせることになるわよ」

頃合いを見計らっていたらしいカリーネが一旦場を締めくくり、脫衣所のテーブルを圍んで話し込んでいた六人は本來の目的である入浴の準備を始めた。

麻里子も自分の服を脫いでいく。シュミーズとストッキングまで脫いだところで自分の身體を見下ろすと、胖次を腰に留めている紐が目に入った。この紐の付き方であるが、紐パンの形をした水著のように真橫に結び目があるわけではない。骨盤の左右の出っ張りのやや內側に當たる所で結ぶようになっている。

（ここなら寢返りを打った時に結び目を敷いて痛い目を見ることが少ないからか。そういえば女物の胖次の縫い目もここにあるのが多かったような氣がするな）

ブラジャーも紐を解いてはずし、最後に殘った胖次の紐に手を掛ける。麻里子はなんとなく今朝の薩妮婭の言葉を思い出した。

（まあ確かに、紐パンの紐を引っ張って解くのは男のロマンの一つなんだろうけど、自分が穿いてるやつの紐を解いても艶っぽくもなんともないな）

「あれっ、麻里子桑紐パンなんだ」

片方の紐を引っ張って蝶結びを解いたところで聲を掛けられた。

「えっ！？　ああ、米卡埃菈桑」

顏を上げると、髮に合わせたのか赤い色のブラと胖次だけになった米卡埃菈が麻里子の方を見ていた。麻里子の見たところ、彼女の胖次はゴム入りの物のようだった。

「ええと、ゴムが通してあるのは高かったので、これしか買えなかったんですよ」

少々情けない話ではあるが事實は事實である。麻里子は困った顏をしながらも素直に告白した。

「え？　なら普通の、ウエストに紐を通してあるやつを買えば良かったのに。服屋桑に無かった？」
「ウエストに、紐？」
「ええと⋯⋯、あ、嚯啦、亞里亞醬が今穿いてるようなやつだよ」

米卡埃菈は少し周りを見回した後、麻里子の隣で服を脫いでいた亞里亞の白い胖次を指して言った。自分の名前が耳に入ったのか、亞里亞の方も麻里子の方に振り返った。

「おねえ醬、どうしたの？」
「亞里亞桑、ちょっと見せてください」

麻里子はその場に膝をついてしゃがみ込むと、亞里亞のウエストに指を這わせた。

「え、きゃ、何？」
「これは⋯⋯」

亞里亞の胖次はウエストを袋縫いにして紐が通してある物だった。胴回りを一周させた紐の兩端を前側にある切れ目から出してくくる形になっている。現代日本で言うと、男性用の海水胖次によくある作りである。

「安い普通の胖次って言ったら、そんなやつのことなんだけど⋯⋯、麻里子桑⋯⋯、もしかして知らなかったの？」

米卡埃菈の言葉に麻里子は答えなかった。そのむき出しの肩がワナワナと小刻みに震えている。

「⋯⋯米蘭達桑？」

やがて發せられた麻里子の低く抑えられた聲に、密かに浴室へ退避しようとしていた米蘭達はその扉の前でしっぽを膨らませて凍りついた。


════════════════════

風呂回と言いつつ、なかなか風呂に入らないのは仕樣です（苦笑）

今回はむしろ胖次回とでも言うべきでしょうか。