# novel

- title: 翼の帰る処シリーズ
- title_zh: 翼之歸處
- author: 妹尾ゆふ子
- illust: ことき
- source: http://www.gentosha-comics.net/event/tsubasa.html
- cover: http://www.gentosha-comics.net/event/images/tsubasa/title_180125.jpg
- publisher:
- date: 2019-12-22T17:49:27+08:00
- status: 已完结
- novel_status: 0x0101

## authors

- 妹尾由布子

## illusts


## publishers

- dmzj

## series

- name: 翼之归处

## preface


```
帝國史官亞爾德擁有『過去視』的能力。體弱多病的他，夢想著能在新的就任地北嶺過上樸素的隱居生活。可天不隨人願，不僅被政治一竅不通的北嶺人弄得暈頭轉向，還被前來就任北嶺太守的好勝皇女折騰得疲於奔命。然而，隨著對於北嶺的了解，亞爾德發現這裡沉睡著帝國的秘密…由歷史光陰編織而就的宏偉浪漫奇譚，正拉開帷幕——

「過去を視る」力を持つ帝国の史官・ヤエト。
病弱な彼は、左遷された赴任先の北嶺で地味な隠居生活を送ることを夢見ていた。
しかし、政治に疎い北嶺の民に悩まされ、さらには北嶺に太守として来た勝ち気な皇女に振り回され、休まる間もない。
だが、北嶺を知るにつれ、ヤエトはこの地に帝国の秘密が眠ることに気づいていく...。
歴史の光陰が織りなす壮大なるファンタジーロマンの扉がいま開かれる――。
```

## tags

- node-novel
- dmzj
- 冒险
- 日本
- 轻之国度

# contribute

- 秒速
- Clsxyz
- macroth
- 江戸前ルナ
- wuling
- 推理之绊12345
- 兴国物语
- 

# options

## dmzj

- novel_id: 534

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: true

# link

- http://www.gentosha-comics.net/event/tsubasa.html
- https://twitter.com/usagi_ya
- https://blog.xuite.net/rosy1827/wretch/318536540-%E6%8E%A8%E8%96%A6+%E7%BF%BC%E4%B9%8B%E6%AD%B8%E8%99%95+%E4%BD%9C%E8%80%85%EF%BC%9A%E5%A6%B9%E5%B0%BE%E5%B8%83%E7%94%B1%E5%AD%90
- 
